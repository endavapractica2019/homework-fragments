package com.example.homeworkmasterdetailflow.okHttp;

import com.example.homeworkmasterdetailflow.models.Genre;
import com.example.homeworkmasterdetailflow.models.TheMovie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {
    @SerializedName("results")
    @Expose
    private List<TheMovie> movies = null;
    private List<Genre> genres = null;

    public List<TheMovie> getResults() {
        return movies;
    }

    public List<Genre> getGenres() {
        return genres;
    }
}
