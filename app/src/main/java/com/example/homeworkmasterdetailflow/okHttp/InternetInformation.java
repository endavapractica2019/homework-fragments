package com.example.homeworkmasterdetailflow.okHttp;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class InternetInformation {

    private static final String mainURL = "https://api.themoviedb.org/3";
    private static final String movieListSortedByPopularity = "/discover/movie?sort_by=popularity.desc";
    private static final String genresList = "/genre/movie/list?";
    private static final String apiKey = "api_key=4d0ab9cf3e108c8ad7d6e7ffde487d45";
    private static final String languageEN = "language=en-US";
    private OkHttpClient okHttpClient;
    private static InternetInformation internetInformation;

    private InternetInformation() {
        okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    public static InternetInformation getInstance() {
        if (internetInformation == null) {
            internetInformation = new InternetInformation();
        }
        return internetInformation;
    }

    public String getMovieListJson() {
        final String movieListURL = mainURL + movieListSortedByPopularity + '&' + apiKey;
        Request request = new Request.Builder()
                .url(movieListURL)
                .build();
        try {
            final String jsonBody = okHttpClient.newCall(request).execute().body().string();
            return jsonBody;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getAllGenresJson() {
        final String genresListURL = mainURL + genresList + apiKey + '&' + languageEN;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(genresListURL)
                .build();

        try {
            return client.newCall(request).execute().body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
