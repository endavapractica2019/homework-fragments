package com.example.homeworkmasterdetailflow.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Entity(tableName = "movies")
public class TheMovie implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    @SerializedName("genre_ids")
    private List<Integer> genre_id;
    @ColumnInfo(name = "original_language")
    @SerializedName("original_language")
    @Expose
    private String originalLanguage;
    private String overview;
    @ColumnInfo(name = "release_date")
    @SerializedName("release_date")
    @Expose
    private Date releaseDate;
    @ColumnInfo(name = "poster_path")
    @SerializedName("poster_path")
    @Expose
    private String posterPath;
    private int starImgId;
    @ColumnInfo(name = "is_favourite")
    private boolean isFavourite;

    public TheMovie() {
        this.isFavourite = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public int getStarImgId() {
        return this.starImgId;
    }

    public void setStarImgId(int id) {
        starImgId = id;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean isFavourite) {
        this.isFavourite = isFavourite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Integer> getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(List<Integer> genre_id) {
        this.genre_id = genre_id;
    }

    public static Comparator<TheMovie> MovieTitleComparatorAscending = new Comparator<TheMovie>() {

        public int compare(TheMovie m1, TheMovie m2) {
            String MovieTitle1 = m1.getTitle().toUpperCase();
            String MovieTitle2 = m2.getTitle().toUpperCase();
            return MovieTitle1.compareTo(MovieTitle2); //ascending order
        }
    };

    public static Comparator<TheMovie> MovieTitleComparatorDescending = new Comparator<TheMovie>() {

        public int compare(TheMovie m1, TheMovie m2) {
            String MovieTitle1 = m1.getTitle().toUpperCase();
            String MovieTitle2 = m2.getTitle().toUpperCase();
            return MovieTitle2.compareTo(MovieTitle1); //descending order
        }
    };

    public static Comparator<TheMovie> MovieReleaseDateComparatorAscending = new Comparator<TheMovie>() {
        @Override
        public int compare(TheMovie m1, TheMovie m2) {
            Date movieDate1 = m1.getReleaseDate();
            Date movieDate2 = m2.getReleaseDate();
            return movieDate1.compareTo(movieDate2);
        }
    };

    public static Comparator<TheMovie> MovieReleaseDateComparatorDescending = new Comparator<TheMovie>() {
        @Override
        public int compare(TheMovie m1, TheMovie m2) {
            Date movieDate1 = m1.getReleaseDate();
            Date movieDate2 = m2.getReleaseDate();
            return movieDate2.compareTo(movieDate1);
        }
    };
}
