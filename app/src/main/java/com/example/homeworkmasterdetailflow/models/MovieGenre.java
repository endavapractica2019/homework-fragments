package com.example.homeworkmasterdetailflow.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "movie_genre",
        primaryKeys = {"movie_id", "genre_id"},
        foreignKeys = {
                @ForeignKey(entity = Genre.class,
                        parentColumns = "id",
                        childColumns = "genre_id",
                        onDelete = CASCADE),

                @ForeignKey(entity = TheMovie.class,
                        parentColumns = "id",
                        childColumns = "movie_id",
                        onDelete = CASCADE)
        })

public class MovieGenre {

    @ColumnInfo(name = "movie_id")
    private int movieId;
    @ColumnInfo(name = "genre_id")
    private int genreId;

    public int getMovieId() {
        return movieId;
    }

    public MovieGenre(int movieId, int genreId) {
        this.movieId = movieId;
        this.genreId = genreId;
    }

    public int getGenreId() {
        return genreId;
    }
}
