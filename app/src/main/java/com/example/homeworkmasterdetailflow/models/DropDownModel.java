package com.example.homeworkmasterdetailflow.models;

public class DropDownModel {

    public String title;
    public int imageRes;

    public DropDownModel(String title, int imageRes) {
        this.title = title;
        this.imageRes = imageRes;
    }
}
