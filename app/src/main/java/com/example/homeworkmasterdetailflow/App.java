package com.example.homeworkmasterdetailflow;

import android.app.Application;

import com.example.homeworkmasterdetailflow.database.MovieRepository;
import com.facebook.stetho.Stetho;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        MovieRepository.getInstance(this);
    }
}
