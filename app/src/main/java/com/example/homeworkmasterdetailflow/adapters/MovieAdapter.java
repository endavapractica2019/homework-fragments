package com.example.homeworkmasterdetailflow.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.homeworkmasterdetailflow.R;
import com.example.homeworkmasterdetailflow.utils.Utils;
import com.example.homeworkmasterdetailflow.database.MovieRepository;
import com.example.homeworkmasterdetailflow.models.TheMovie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> {

    private List<TheMovie> movies;
    private OnMovieClickedListener movieListener;
    private OnStarClickedListener starListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public View view;
        @BindView(R.id.title_movie_row_id)
        TextView titleTV;
        @BindView(R.id.release_date_movie_row_id)
        TextView releaseDateTV;
        @BindView(R.id.item_image_id)
        ImageView movieImgIV;
        @BindView(R.id.star_imageButton_id)
        ImageButton starImgIB;
        @BindView(R.id.linearLayout_movie_row_id)
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
        }
    }

    public MovieAdapter(List<TheMovie> movies, OnMovieClickedListener movieListener, OnStarClickedListener starListener) {
        this.movieListener = movieListener;
        this.starListener = starListener;
        this.movies = movies;
    }

    @NonNull
    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieAdapter.MyViewHolder holder, final int position) {
        final TheMovie movie = movies.get(position);

        if (!movie.isFavourite()) {
            holder.starImgIB.setImageResource(R.drawable.ic_empty_star);
        } else {
            holder.starImgIB.setImageResource(R.drawable.ic_golden_star);
        }
        holder.titleTV.setText(movie.getTitle());

        Glide.with(holder.view)
                .load("http://image.tmdb.org/t/p/w185/" + movie.getPosterPath())
                .into(holder.movieImgIV);

        holder.releaseDateTV.setText(Utils.dateFormat(movie.getReleaseDate()));

        holder.starImgIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movies.get(position).setFavourite(!movies.get(position).isFavourite());
                if (movies.get(position).isFavourite()) {
                    movies.get(position).setStarImgId(R.drawable.ic_golden_star);
                } else {
                    movies.get(position).setStarImgId(R.drawable.ic_empty_star);
                }
                starListener.onStarClicked(position);
                holder.starImgIB.setImageResource(movie.getStarImgId());
                Context context = view.getContext();

                new AsyncTask<TheMovie, Void, Void>() {

                    @Override
                    protected Void doInBackground(TheMovie... theMovies) {
                        MovieRepository.getInstance(context).updateOneMovieRow(theMovies[0]);
                        return null;
                    }
                }.execute(movies.get(position));
            }
        });

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movieListener.onMovieClicked(position);
            }
        });

        holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                movieListener.onMovieLongClicked(position);
                return true;
            }
        });
    }

    public void addItems(List<TheMovie> list) {
        movies.clear();
        movies.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public interface OnMovieClickedListener {
        void onMovieClicked(int position);

        void onMovieLongClicked(int position);
    }

    public interface OnStarClickedListener {
        void onStarClicked(int position);
    }
}