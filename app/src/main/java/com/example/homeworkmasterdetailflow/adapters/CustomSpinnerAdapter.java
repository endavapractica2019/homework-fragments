package com.example.homeworkmasterdetailflow.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.homeworkmasterdetailflow.R;
import com.example.homeworkmasterdetailflow.events.EventFilterAlph;
import com.example.homeworkmasterdetailflow.events.EventFilterGenre;
import com.example.homeworkmasterdetailflow.events.EventFilterTime;
import com.example.homeworkmasterdetailflow.events.EventfilterLanguage;
import com.example.homeworkmasterdetailflow.models.DropDownModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    Context context;
    final ArrayList<DropDownModel> dropDownModels;

    public CustomSpinnerAdapter(Context context, ArrayList<DropDownModel> entries) {
        this.context = context;
        this.dropDownModels = entries;
    }

    @Override
    public int getCount() {
        return dropDownModels.size();
    }

    @Override
    public Object getItem(int i) {
        return dropDownModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.spinner_layout, null);
        ImageView filterIcon = v.findViewById(R.id.spinner_filter_icon_id);
        filterIcon.setImageResource(R.drawable.ic_filter);
        return v;
    }

    @Override
    public View getDropDownView(int position, View v, ViewGroup parent) {
        View view = View.inflate(context, R.layout.spinner_entries, null);
        LinearLayout linearLayout = view.findViewById(R.id.dropdown_item_id);
        final TextView textView = view.findViewById(R.id.filter_textview_id);
        final ImageView imageview = view.findViewById(R.id.filter_imageview_id);

        imageview.setImageResource(dropDownModels.get(position).imageRes);
        textView.setText(dropDownModels.get(position).title);

        switch (position) {
            case 0:
                linearLayout.setOnClickListener(view1 -> EventBus.getDefault().post(new EventFilterAlph()));
                break;
            case 1:
                linearLayout.setOnClickListener(view1 -> EventBus.getDefault().post(new EventFilterTime()));
                break;
            case 2:
                linearLayout.setOnClickListener(view1 -> EventBus.getDefault().post(new EventfilterLanguage()));
                break;
            case 3:
                linearLayout.setOnClickListener(view1 -> EventBus.getDefault().post(new EventFilterGenre()));
                break;
            default:
        }
        return view;
    }
}
