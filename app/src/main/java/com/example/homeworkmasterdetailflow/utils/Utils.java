package com.example.homeworkmasterdetailflow.utils;

import com.example.homeworkmasterdetailflow.models.Genre;
import com.example.homeworkmasterdetailflow.okHttp.InternetInformation;
import com.example.homeworkmasterdetailflow.okHttp.Response;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Utils {

    public static String dateFormat(Date date) {
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat outputformat = new SimpleDateFormat(outputPattern);
        return outputformat.format(date);
    }

    public static List<Genre> getAllGenres() {
        Gson gson = new Gson();
        final String s = InternetInformation.getInstance().getAllGenresJson();
        if (s == null && s.equals("")) {
            return null;
        }
        List<Genre> genresList = (gson.fromJson(s, Response.class).getGenres());
        return genresList;
    }
}
