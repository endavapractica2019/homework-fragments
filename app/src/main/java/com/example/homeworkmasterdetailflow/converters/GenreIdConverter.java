package com.example.homeworkmasterdetailflow.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class GenreIdConverter {

    static Gson gson = new Gson();

    @TypeConverter
    public static List<Integer> stringToIntArray(String data) {
        if (data == null) {
            return Collections.emptyList();
        }
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String intArrayToString(List<Integer> ids) {
        return gson.toJson(ids);
    }
}
