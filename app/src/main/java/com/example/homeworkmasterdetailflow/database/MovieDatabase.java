package com.example.homeworkmasterdetailflow.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.homeworkmasterdetailflow.converters.GenreIdConverter;
import com.example.homeworkmasterdetailflow.models.Genre;
import com.example.homeworkmasterdetailflow.models.MovieGenre;
import com.example.homeworkmasterdetailflow.models.TheMovie;
import com.example.homeworkmasterdetailflow.converters.DateConverter;

@Database(entities = {TheMovie.class, Genre.class, MovieGenre.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class, GenreIdConverter.class})
public abstract class MovieDatabase extends RoomDatabase {

    public abstract DaoAccess daoAccess();
}
