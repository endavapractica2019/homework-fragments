package com.example.homeworkmasterdetailflow.database;

import android.content.Context;

import androidx.room.Room;

import com.example.homeworkmasterdetailflow.models.Genre;
import com.example.homeworkmasterdetailflow.models.MovieGenre;
import com.example.homeworkmasterdetailflow.models.TheMovie;

import java.util.List;

public class MovieRepository {

    private String DB_NAME = "db_movie";
    private MovieDatabase movieDatabase;
    private static MovieRepository movieRepository;

    private MovieRepository(Context context) {
        movieDatabase = Room.databaseBuilder(context, MovieDatabase.class, DB_NAME).build();
    }

    public static MovieRepository getInstance(final Context context) {
        if (movieRepository == null) {
            movieRepository = new MovieRepository(context);
        }
        return movieRepository;
    }

    public void insertOneMovieGenreItem(MovieGenre movieGenre) {
        movieDatabase.daoAccess().insertOneMovieGenreItem(movieGenre);
    }

    public void insertAllMovies(List<TheMovie> movies) {
        movieDatabase.daoAccess().insertAllMovies(movies);
    }

    public List<TheMovie> getAllMovies() {
        return movieDatabase.daoAccess().fetchAllMovies();
    }

    public void insertAllGenres(List<Genre> genresList) {
        movieDatabase.daoAccess().insertAllGenres(genresList);
    }

    public List<Genre> getAllGenres() {
        return movieDatabase.daoAccess().fetchAllGenres();
    }

    public List<TheMovie> getMoviesByKeyword(String keyword) {
        return movieDatabase.daoAccess().getMoviesByKeyword(keyword);
    }

    public void updateOneMovieRow(TheMovie movie) {
        movieDatabase.daoAccess().updateOneMovie(movie);
    }

    public void deleteOneMovie(TheMovie movie) {
        movieDatabase.daoAccess().deleteMovie(movie);
    }
}
