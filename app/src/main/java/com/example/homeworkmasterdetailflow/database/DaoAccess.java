package com.example.homeworkmasterdetailflow.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.homeworkmasterdetailflow.models.Genre;
import com.example.homeworkmasterdetailflow.models.MovieGenre;
import com.example.homeworkmasterdetailflow.models.TheMovie;

import java.util.List;

@Dao
public interface DaoAccess {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllMovies(List<TheMovie> listOfMovies);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllGenres(List<Genre> genresList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOneMovieGenreItem(MovieGenre movieGenre);

    @Query("SELECT * FROM movies")
    List<TheMovie> fetchAllMovies();

    @Query("SELECT * FROM genres")
    List<Genre> fetchAllGenres();

    @Query("SELECT * FROM movies WHERE title LIKE '%' || :keyword || '%'")
    List<TheMovie> getMoviesByKeyword(String keyword);

    @Update
    void updateOneMovie(TheMovie movie);

    @Delete
    void deleteMovie(TheMovie movie);
}
