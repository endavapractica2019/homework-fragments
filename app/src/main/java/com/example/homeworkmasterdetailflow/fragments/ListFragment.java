package com.example.homeworkmasterdetailflow.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homeworkmasterdetailflow.R;
import com.example.homeworkmasterdetailflow.utils.Utils;
import com.example.homeworkmasterdetailflow.adapters.MovieAdapter;
import com.example.homeworkmasterdetailflow.database.MovieRepository;
import com.example.homeworkmasterdetailflow.events.EventFilterAlph;
import com.example.homeworkmasterdetailflow.events.EventFilterGenre;
import com.example.homeworkmasterdetailflow.events.EventFilterTime;
import com.example.homeworkmasterdetailflow.events.EventfilterLanguage;
import com.example.homeworkmasterdetailflow.models.Genre;
import com.example.homeworkmasterdetailflow.models.MovieGenre;
import com.example.homeworkmasterdetailflow.models.TheMovie;
import com.example.homeworkmasterdetailflow.okHttp.InternetInformation;
import com.example.homeworkmasterdetailflow.okHttp.Response;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ListFragment extends Fragment {

    private List<TheMovie> movies = new ArrayList<>();
    private List<Genre> genres = new ArrayList<>();
    private MovieAdapter movieAdapter;
    private FloatingActionButton addAMovieFAB;
    private RecyclerView recyclerView;
    private MovieAdapter.OnMovieClickedListener movieClickedListener;
    private MovieAdapter.OnStarClickedListener starClickedListener;
    private OnAddFABClickedListener onAddFABClickedListener;
    private PassingIntent passingIntent;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        addAMovieFAB = view.findViewById(R.id.add_movie_floating_button);
        movieClickedListener = new MovieAdapter.OnMovieClickedListener() {
            @Override
            public void onMovieClicked(int position) {
                passingIntent.passingMovie(movies.get(position));
            }

            @Override
            public void onMovieLongClicked(int position) {
                DeleteDataAsync deleteDataAsync = new DeleteDataAsync();
                deleteDataAsync.execute(movies.get(position));
            }
        };

        starClickedListener = position -> {
        };

        addAMovieFAB.setOnClickListener(view1 -> onAddFABClickedListener.onFABClicked());

        recyclerView = view.findViewById(R.id.recycler_view_id);
        movieAdapter = new MovieAdapter(movies, movieClickedListener, starClickedListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(movieAdapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        passingIntent = (PassingIntent) context;
        onAddFABClickedListener = (OnAddFABClickedListener) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        PrepareDataAsync prepareDataAsync = new PrepareDataAsync();
        prepareDataAsync.execute();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void receiveAlphFilter(EventFilterAlph event) {

        new AsyncTask<Void, Void, List<TheMovie>>() {

            @Override
            protected List<TheMovie> doInBackground(Void... voids) {
                return MovieRepository.getInstance(getActivity()).getAllMovies();
            }

            @Override
            protected void onPostExecute(List<TheMovie> theMovies) {
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
                alt_bld.setTitle("Alphabetic");
                alt_bld.setSingleChoiceItems(new String[]{"From A to Z", "From Z to A"}, -1, (dialog, item) -> {
                    if (item == 0) {
                        Collections.sort(theMovies, TheMovie.MovieTitleComparatorAscending);
                    } else {
                        Collections.sort(theMovies, TheMovie.MovieTitleComparatorDescending);
                    }
                    movieAdapter.addItems(theMovies);
                    dialog.dismiss();
                });
                alt_bld.create().show();
            }
        }.execute();
    }


    @Subscribe
    public void receiveTimeFilter(EventFilterTime event) {

        new AsyncTask<Void, Void, List<TheMovie>>() {

            @Override
            protected List<TheMovie> doInBackground(Void... voids) {
                return MovieRepository.getInstance(getActivity()).getAllMovies();
            }

            @Override
            protected void onPostExecute(List<TheMovie> theMovies) {
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
                alt_bld.setTitle("Release date");
                alt_bld.setSingleChoiceItems(new String[]{"Ascending by release date", "Descending by release date"}, -1, (dialog, item) -> {
                    if (item == 0) {
                        Collections.sort(theMovies, TheMovie.MovieReleaseDateComparatorAscending);
                    } else {
                        Collections.sort(theMovies, TheMovie.MovieReleaseDateComparatorDescending);
                    }
                    movieAdapter.addItems(theMovies);
                    dialog.dismiss();
                });
                alt_bld.create().show();
            }
        }.execute();
    }

    @Subscribe
    public void receiveLanguage(EventfilterLanguage event) {
        new AsyncTask<Void, Void, List<TheMovie>>() {

            @Override
            protected List<TheMovie> doInBackground(Void... voids) {
                return MovieRepository.getInstance(getActivity()).getAllMovies();
            }

            @Override
            protected void onPostExecute(List<TheMovie> theMovies) {
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
                alt_bld.setTitle("Language");
                alt_bld.setSingleChoiceItems(new String[]{"Original language: EN", "Original language: Other"}, -1, (dialog, item) -> {
                    List<TheMovie> filteredMovies = new ArrayList<>();
                    if (item == 0) {
                        for (TheMovie m : theMovies) {
                            if (m.getOriginalLanguage().equals("en")) {
                                filteredMovies.add(m);
                            }
                        }
                    } else {
                        for (TheMovie m : theMovies) {
                            if (!m.getOriginalLanguage().equals("en")) {
                                filteredMovies.add(m);
                            }
                        }
                    }
                    movieAdapter.addItems(filteredMovies);
                    dialog.dismiss();
                });
                alt_bld.create().show();
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    @Subscribe
    public void receiveGenre(EventFilterGenre event) {
        new AsyncTask<Void, Void, HashMap<String, Integer>>() {

            @Override
            protected HashMap<String, Integer> doInBackground(Void... voids) {
                List<Genre> genres = MovieRepository.getInstance(getActivity()).getAllGenres();
                movies = MovieRepository.getInstance(getActivity()).getAllMovies();
                HashMap<String, Integer> genreMap = new HashMap<>();
                for (Genre g : genres) {
                    genreMap.put(g.getGenreName(), g.getId());
                }
                return genreMap;
            }

            @Override
            protected void onPostExecute(HashMap<String, Integer> map) {
                AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
                alt_bld.setTitle("Genre");
                String[] genresName = new String[map.keySet().size()];
                int i = 0;
                for (String key : map.keySet()) {
                    genresName[i] = key;
                    i++;
                }
                alt_bld.setSingleChoiceItems(genresName, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        List<TheMovie> listOfMoviesToDisplay = new ArrayList<>();
                        int selectedGenreId = map.get(genresName[i]);
                        for (TheMovie m : movies) {
                            if (m.getGenre_id().contains(selectedGenreId)) {
                                listOfMoviesToDisplay.add(m);
                            }
                        }
                        movieAdapter.addItems(listOfMoviesToDisplay);
                        dialogInterface.dismiss();
                    }
                });
                alt_bld.create().show();
            }
        }.execute();
    }

    public interface PassingIntent {
        void passingMovie(TheMovie movie);
    }

    public interface OnAddFABClickedListener {
        void onFABClicked();
    }

    public class PrepareDataAsync extends AsyncTask<Void, Void, List<TheMovie>> {

        @Override
        protected List<TheMovie> doInBackground(Void... voids) {
            genres = Utils.getAllGenres();
            MovieRepository.getInstance(getActivity()).insertAllGenres(genres);
            Gson gson = new Gson();
            final String s = InternetInformation.getInstance().getMovieListJson();
            if (s == null && s.equals("")) {
                return null;
            }
            List<TheMovie> newMovieList = (gson.fromJson(s, Response.class)).getResults();
            MovieRepository.getInstance(getActivity()).insertAllMovies(newMovieList);
            List<TheMovie> databaseMovies = MovieRepository.getInstance(getActivity()).getAllMovies();
            initTheMovieGenreTable(databaseMovies);
            return databaseMovies;
        }

        @Override
        protected void onPostExecute(List<TheMovie> s) {
            super.onPostExecute(s);
            movieAdapter.addItems(s);
        }
    }

    public void initTheMovieGenreTable(List<TheMovie> movies) {
        List<Integer> ids;
        for (TheMovie m : movies) {
            ids = m.getGenre_id();
            for (int i = 0; i < ids.size(); i++) {
                MovieRepository.getInstance(getActivity()).insertOneMovieGenreItem(new MovieGenre(m.getId(), ids.get(i).intValue()));
            }
        }
    }

    public void getMovieListByKeyWord(String keyword) {
        new AsyncTask<String, Void, List<TheMovie>>() {

            @Override
            protected List<TheMovie> doInBackground(String... strings) {
                return MovieRepository.getInstance(getActivity()).getMoviesByKeyword(strings[0]);
            }

            @Override
            protected void onPostExecute(List<TheMovie> list) {
                super.onPostExecute(list);
                movieAdapter.addItems(list);
            }
        }.execute(keyword);
    }

    public class DeleteDataAsync extends AsyncTask<TheMovie, Void, List<TheMovie>> {

        @Override
        protected List<TheMovie> doInBackground(TheMovie... movies) {
            MovieRepository.getInstance(getActivity()).deleteOneMovie(movies[0]);
            return MovieRepository.getInstance(getActivity()).getAllMovies();
        }

        @Override
        protected void onPostExecute(List<TheMovie> moviesRemained) {
            movieAdapter.addItems(moviesRemained);
        }
    }
}
