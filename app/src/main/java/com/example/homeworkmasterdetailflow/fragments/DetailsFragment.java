package com.example.homeworkmasterdetailflow.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.homeworkmasterdetailflow.R;
import com.example.homeworkmasterdetailflow.models.TheMovie;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsFragment extends Fragment {

    @BindView(R.id.details_imageView_id)
    ImageView posterIV;
    @BindView(R.id.details_title_id)
    TextView titleTV;
    @BindView(R.id.details_overview_textView_id)
    TextView detailsTV;
    @BindView(R.id.details_original_language_id)
    TextView originalLanguageTV;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void setContent(TheMovie movie) {
        Glide.with(getActivity())
                .load("http://image.tmdb.org/t/p/w185/" + movie.getPosterPath())
                .into(posterIV);
        titleTV.setText(movie.getTitle());
        originalLanguageTV.setText(movie.getOriginalLanguage());
        detailsTV.setText(movie.getOverview());
    }
}