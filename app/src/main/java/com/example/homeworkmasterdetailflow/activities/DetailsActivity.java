package com.example.homeworkmasterdetailflow.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.homeworkmasterdetailflow.R;
import com.example.homeworkmasterdetailflow.fragments.DetailsFragment;
import com.example.homeworkmasterdetailflow.models.TheMovie;

import static com.example.homeworkmasterdetailflow.activities.MainActivity.movieString;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        TheMovie movie = (TheMovie) getIntent().getSerializableExtra(movieString);
        DetailsFragment detailsFragment = (DetailsFragment) getSupportFragmentManager().findFragmentById(R.id.details_fragment);
        if (detailsFragment != null) {
            detailsFragment.setContent(movie);
        }
    }
}
