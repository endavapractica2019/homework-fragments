package com.example.homeworkmasterdetailflow.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.homeworkmasterdetailflow.R;
//import com.example.homeworkmasterdetailflow.SharedPreferencesHelper;
import com.example.homeworkmasterdetailflow.adapters.CustomSpinnerAdapter;
import com.example.homeworkmasterdetailflow.fragments.DetailsFragment;
import com.example.homeworkmasterdetailflow.fragments.ListFragment;
import com.example.homeworkmasterdetailflow.models.DropDownModel;
import com.example.homeworkmasterdetailflow.models.TheMovie;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ListFragment.PassingIntent, ListFragment.OnAddFABClickedListener {

    private DetailsFragment detailsFragment;
    private ListFragment listFragment;
    @BindView(R.id.mySpinner)
    Spinner filterSpinner;
    @BindView(R.id.search_edit_text_id)
    EditText searchMovie;
    public static final String movieString = "movie";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initDropdown();

        if (getResources().getBoolean(R.bool.isLandscape)) {
            FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
            detailsFragment = new DetailsFragment();
            fm.add(R.id.landscape_frameLayout_id, detailsFragment);
            fm.addToBackStack(null);
            fm.commit();
        } else {
            FragmentTransaction fm = getSupportFragmentManager().beginTransaction();
            listFragment = new ListFragment();
            fm.add(R.id.portrait_fragment_container_id, listFragment);
            fm.addToBackStack(null);
            fm.commit();
        }
        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                searchMovie.clearFocus();
            }
        });

        searchMovie.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listFragment.getMovieListByKeyWord(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public void initDropdown() {
        final ArrayList<DropDownModel> dropDownModels = new ArrayList<>();
        dropDownModels.add(new DropDownModel("Alphabetic", R.drawable.ic_a_z));
        dropDownModels.add(new DropDownModel("Time", R.drawable.ic_date));
        dropDownModels.add(new DropDownModel("Language", R.drawable.ic_language));
        dropDownModels.add(new DropDownModel("Genre", R.drawable.ic_genre));

        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this, dropDownModels);
        filterSpinner.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void passingMovie(TheMovie movie) {
        if (getResources().getBoolean(R.bool.isLandscape)) {
            detailsFragment.setContent(movie);
        } else {
            Intent intent = new Intent(this, DetailsActivity.class);
            intent.putExtra(movieString, movie);
            startActivity(intent);
        }
    }

    @Override
    public void onFABClicked() {
    }
}